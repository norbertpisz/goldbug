﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoldBug.Data;
using GoldBug.Models;
using Microsoft.AspNetCore.Mvc;


namespace GoldBug.Controllers
{
    public class API_UserSitesController : Controller
    {
        private readonly ApplicationDbContext _db;

        public API_UserSitesController(ApplicationDbContext db)
        {
            _db = db;
        }
        
        [Route("api/user-sites/add")]
        public IActionResult AddUserSite(string domain)
        {
            if (domain == null)
                return Json(new { error = 1 });

            if (domain.Contains("http://"))
                domain = domain.Replace("http://", "");

            if (domain.Contains("https://"))
                domain = domain.Replace("https://", "");

            if (!domain.StartsWith("www"))
                domain = "www." + domain;

            if (domain.Contains("/"))
            {
                var arr = domain.Split("/");
                domain = arr[0];
            }

            UserSiteM site = _db.UserSites.SingleOrDefault(a => a.Domain == domain);
            if (site == null)
            {
                site = new UserSiteM();
                site.Domain = domain;
                site.Views = 1;
                site.LastViewed = DateTime.Now;
                site.TrackingScripts = new List<string>() { "ga" };

                _db.Add(site);
                _db.SaveChanges();
            }
            else
            {
                site.LastViewed = DateTime.Now;
                site.Views = site.Views + 1;
                _db.Update(site);
                _db.SaveChanges();
            }

            var rules = _db.SiteSecurityRules.Where(a => a.UserSiteId == site.Id);
            List<string> sRules = new List<string>();


            return Json(new { error = false , rules=sRules});
        }

        [Route("api/user-sites/rules")]
        public IActionResult GetSiteConfig(string domain)
        {
            var rules = _db.SiteSecurityRules.ToList();

            return Json(new {error=false,data=rules });
        }
    }
}
