﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoldBug.Data;
using GoldBug.Models;
using Microsoft.AspNetCore.Mvc;

namespace GoldBug.Controllers
{
	public class WordSearchController : Controller
	{
		private readonly ApplicationDbContext _db;

		public WordSearchController(ApplicationDbContext db)
		{
			_db = db;
		}

		[Route("user/my-word-search")]
		public IActionResult MyWordSearch()
		{
			return View();
		}

		[Route("user/my-word-search/init")]
		public IActionResult Init()
		{
			var wordSearchList = new List<WordSearchM>();
			wordSearchList.Add(new WordSearchM() { Site = "www.morele.net", Name = "MSI", Count = 256 });
			wordSearchList.Add(new WordSearchM() { Site = "www.morele.net", Name = "Laptop", Count = 321 });
			wordSearchList.Add(new WordSearchM() { Site = "www.itaka.pl", Name = "Chorwacja", Count = 542 });
			wordSearchList.Add(new WordSearchM() { Site = "www.itaka.pl", Name = "Wakacje", Count = 452 });
			wordSearchList.Add(new WordSearchM() { Site = "www.morele.net", Name = "SSD", Count = 77 });
			wordSearchList.Add(new WordSearchM() { Site = "www.morele.net", Name = "Xiaomi", Count = 153 });
			wordSearchList.Add(new WordSearchM() { Site = "www.pyszne.pl", Name = "Zupa", Count = 40 });
			wordSearchList.Add(new WordSearchM() { Site = "www.itaka.pl", Name = "Grecja", Count = 85 });
			wordSearchList.Add(new WordSearchM() { Site = "www.pyszne.pl", Name = "Lunch", Count = 100 });
			wordSearchList.Add(new WordSearchM() { Site = "www.pyszne.pl", Name = "Proza", Count = 45 });
			wordSearchList.Add(new WordSearchM() { Site = "www.pyszne.pl", Name = "Restaurant", Count = 327 });
			wordSearchList.Add(new WordSearchM() { Site = "www.morele.net", Name = "HDD", Count = 54 });
			wordSearchList.Add(new WordSearchM() { Site = "www.itaka.pl", Name = "Władysławowo", Count = 4 });

			foreach (var s in wordSearchList)
			{
				WordSearchM so = _db.WordSearches.SingleOrDefault(a => a.Name == s.Name);
				if (so == null)
				{
					_db.Add(s);
					_db.SaveChanges();
				}
			}

			return RedirectToAction(nameof(MyWordSearch));
		}

		[Route("user/my-word-search/update")]
		public IActionResult Update()
		{
			var wordSearchList = _db.WordSearches.ToList();
			foreach (var s in wordSearchList)
			{
				var site = _db.UserSites.FirstOrDefault(a => a.Domain == s.Site);
				if (site != null)
				{
					s.UserSiteId = site.Id;
					_db.Update(s);
					_db.SaveChanges();
				}

			}
			return RedirectToAction(nameof(MyWordSearch));
		}
	}
}