﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace GoldBug.Controllers
{
    public class UserSettingsController : Controller
    {
       
        [Route("user/settings")]
        public IActionResult ViewUserSettings()
        {
            return View();
        }
    }
}
