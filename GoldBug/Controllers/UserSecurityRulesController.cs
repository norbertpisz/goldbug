﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoldBug.Data;
using Microsoft.AspNetCore.Mvc;


namespace GoldBug.Controllers
{
    public class UserSecurityRulesController : Controller
    {
		private readonly ApplicationDbContext _db;

		public UserSecurityRulesController(ApplicationDbContext db)
		{
			_db = db;
		}

		[Route("user/security-rules")]
        public IActionResult ViewMySecurityRules()
        {
            return View();
        }  
  
		[Route("user/security-rules/delete-all")]
        public IActionResult DeleteMySecurityRules()
        {
			var dbRules = _db.SiteSecurityRules.ToList();
			foreach (var s in dbRules)
			{
				_db.Remove(s);
				_db.SaveChanges();
			}
			return RedirectToAction(nameof(ViewMySecurityRules));
		}
    }
}
