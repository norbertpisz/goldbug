﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace GoldBug.Controllers
{
    public class UserStatsController : Controller
    {
        
        [Route("user/stats")]
        public IActionResult ViewUserStats()
        {
            return View();
        }
    }
}
