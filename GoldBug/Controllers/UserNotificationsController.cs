﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoldBug.Data;
using GoldBug.Models;
using Microsoft.AspNetCore.Mvc;

namespace GoldBug.Controllers
{
	public class UserNotificationsController : Controller
	{
		private readonly ApplicationDbContext _db;

		public UserNotificationsController(ApplicationDbContext db)
		{
			_db = db;
		}

		[Route("user/mynotifications")]
		public IActionResult ViewMyNotifications()
		{
			return View();
		}

		[Route("user/mynotifications/init")]
		public IActionResult Init()
		{
			var notificationsList = new List<NotificationM>();
			notificationsList.Add(new NotificationM() { Action = "Set false data", Name = "Google Analytics blocked", Site = "www.facebook.com" });
			notificationsList.Add(new NotificationM() { Action = "Facebook Connect blocked", Name = "Website use tracking pixel", Site = "www.onet.pl" });
			notificationsList.Add(new NotificationM() { Action = "Google Publisher Tags blocked", Name = "Google Analytics blocked", Site = "www.itaka.pl" });
			notificationsList.Add(new NotificationM() { Action = "Facebook Connect blocked", Name = "Website get Your Location", Site = "www.itaka.pl" });
			notificationsList.Add(new NotificationM() { Action = "Google Adsense blocked", Name = "Website use tracking pixel", Site = "app.hrappka.pl" });
			notificationsList.Add(new NotificationM() { Action = "Google Adsense blocked", Name = "Website use Google Analytics", Site = "www.morele.net" });
			notificationsList.Add(new NotificationM() { Action = "Facebook Connect blocked", Name = "Share Location blocked", Site = "www.pyszne.pl" });
			foreach (var s in notificationsList)
			{
					_db.Add(s);
					_db.SaveChanges();
			}
			return RedirectToAction(nameof(ViewMyNotifications));
		}

		[Route("user/mynotifications/update")]
		public IActionResult Update()
		{
			var notificationsList = _db.Notifications.ToList();
			foreach (var s in notificationsList)
			{
				var site = _db.UserSites.FirstOrDefault(a => a.Domain == s.Site);
				if(site != null)
				{
					s.UserSiteId = site.Id;
					_db.Update(s);
					_db.SaveChanges();
				}
				
			}
			return RedirectToAction(nameof(ViewMyNotifications));
		}

	}
}