﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoldBug.Data;
using GoldBug.Models;
using Microsoft.AspNetCore.Mvc;


namespace GoldBug.Controllers
{
	public class UserMySitesController : Controller
	{
		private readonly ApplicationDbContext _db;

		public UserMySitesController(ApplicationDbContext db)
		{
			_db = db;
		}

		[Route("user/mysites")]
		public IActionResult ViewMySties()
		{
			return View();
		}

        public IActionResult AddSiteRule(string name, string ruleCode, string siteName, long siteId, string trackerCode, string RuleRegex)
        {
            SecurityRuleM rule = _db.SiteSecurityRules.SingleOrDefault(a => a.RuleCode == ruleCode && a.UserSiteId == siteId);
            if (rule == null)
            {
                rule = new SecurityRuleM();
                rule.RuleCode = ruleCode;
                rule.Name = name;
                rule.UserSiteId = siteId;
                rule.Site = siteName;
                rule.TrackerCode = trackerCode;
                rule.RuleRegex = RuleRegex;

                _db.Add(rule);
                _db.SaveChanges();
            }
            else
            {
                rule.RuleRegex = RuleRegex;


                _db.Update(rule);
                _db.SaveChanges();
            }


            return RedirectToAction(nameof(ViewMySite), new { siteId = siteId });
        }       
  
		public IActionResult RemoveSiteRule(string name, string ruleCode, string siteName, long siteId, string trackerCode)
        {
            SecurityRuleM rule = _db.SiteSecurityRules.SingleOrDefault(a => a.RuleCode == ruleCode && a.UserSiteId == siteId);
            if (rule != null)
            {
                _db.Remove(rule);
                _db.SaveChanges();
            }


            return RedirectToAction(nameof(ViewMySite), new { siteId = siteId });
        }

		[Route("user/mysites/site")]
		public IActionResult ViewMySite(long siteId)
		{
			return View(siteId);
		}

		[Route("user/mysites/init")]
		public IActionResult Init()
		{
			var userSites = new List<UserSiteM>();
			userSites.Add(new UserSiteM()
			{
				Domain = "www.facebook.com",
				DomainCategory = "Social",
				TrackingScripts = new List<string>() { "fb", "loc" },
				ListData = new List<string>() { "user", "loc", "chat" },
				LastVisited = "10 minutes ago",
				Views = 27,
			});
			userSites.Add(new UserSiteM()
			{
				Domain = "www.onet.pl",
				DomainCategory = "Press",
				TrackingScripts = new List<string>() { "ga", "fb", },
				ListData = new List<string>() { "user", "chat" },
				LastVisited = "1 hours ago",
				Views = 33,
			});
			userSites.Add(new UserSiteM()
			{
				Domain = "www.wykop.pl",
				DomainCategory = "Press & Social",
				TrackingScripts = new List<string>() { "ga", "fb" },
				ListData = new List<string>() { "loc", "chat" },
				LastVisited = "7 minute ago",
				Views = 12,
			});
			userSites.Add(new UserSiteM()
			{
				Domain = "www.x-kom.pl",
				DomainCategory = "Shop",
				TrackingScripts = new List<string>() { "ga", "loc" },
				ListData = new List<string>() { "chat" },
				LastVisited = "3 days ago",
				Views = 2,
			});
			userSites.Add(new UserSiteM()
			{
				Domain = "www.spotify.com",
				DomainCategory = "Music",
				TrackingScripts = new List<string>() { "ga", "fb" },
				ListData = new List<string>() { "user", "loc", "chat" },
				LastVisited = "12 hours ago",
				Views = 123,
			});
			userSites.Add(new UserSiteM()
			{
				Domain = "www.itaka.pl",
				DomainCategory = "Travel",
				TrackingScripts = new List<string>() { "ga", "fb", "loc" },
				ListData = new List<string>() { "loc", "chat" },
				LastVisited = "57 minutes ago",
				Views = 3,
			});
			userSites.Add(new UserSiteM()
			{
				Domain = "www.allegro.pl",
				DomainCategory = "Shop",
				TrackingScripts = new List<string>() { "ga", "fb" },
				ListData = new List<string>() { "user", "chat" },
				LastVisited = "7 hours ago",
				Views = 15,
			});
			userSites.Add(new UserSiteM()
			{
				Domain = "app.hrappka.pl",
				DomainCategory = "Social",
				TrackingScripts = new List<string>() { "fb" },
				ListData = new List<string>() { "user", "loc", "chat" },
				LastVisited = "15 hours ago",
				Views = 78,
			});
			userSites.Add(new UserSiteM()
			{
				Domain = "www.olx.pl",
				DomainCategory = "Shop",
				TrackingScripts = new List<string>() { "fb", "loc" },
				ListData = new List<string>() { "user", "loc", "chat" },
				LastVisited = "5 minute ago",
				Views = 56,
			});
			userSites.Add(new UserSiteM()
			{
				Domain = " www.otomoto.pl",
				DomainCategory = "Shop",
				TrackingScripts = new List<string>() { "fb", "loc" },
				ListData = new List<string>() { "user" },
				LastVisited = "23 hours ago",
				Views = 13,
			});

			userSites.Add(new UserSiteM()
			{
				Domain = "www.morele.net",
				DomainCategory = "Shop",
				TrackingScripts = new List<string>() { "ga", "fb" },
				ListData = new List<string>() { "user", "loc" },
				LastVisited = "1 day ago",
				Views = 7,
			});
			userSites.Add(new UserSiteM()
			{
				Domain = "www.travelplanet.pl",
				DomainCategory = "Travel",
				TrackingScripts = new List<string>() { "ga", "loc" },
				ListData = new List<string>() { "user", "loc", "chat" },
				LastVisited = "30 minutes ago",
				Views = 65,
			});
			userSites.Add(new UserSiteM()
			{
				Domain = "www.netflix.com",
				DomainCategory = "Movie",
				TrackingScripts = new List<string>() { "ga", "fb" },
				ListData = new List<string>() { "user", "chat" },
				LastVisited = "6 hours ago",
				Views = 34,
			});
			userSites.Add(new UserSiteM()
			{
				Domain = "www.pyszne.pl",
				DomainCategory = "Food",
				TrackingScripts = new List<string>() { "fb", "loc" },
				ListData = new List<string>() { "user", "loc", "chat" },
				LastVisited = "20 minutes ago",
				Views = 9,
			});
			userSites.Add(new UserSiteM()
			{

				Domain = "www.lunchup.pl",
				DomainCategory = "Food",
				TrackingScripts = new List<string>() { "ga", "loc" },
				ListData = new List<string>() { "loc", "chat" },
				LastVisited = "1 minute ago",
				Views = 227,
			});


			foreach (var s in userSites)
			{
				UserSiteM so = _db.UserSites.SingleOrDefault(a => a.Domain == s.Domain);
				if (so == null)
				{
					_db.Add(s);
					_db.SaveChanges();
				}
			}

			return RedirectToAction(nameof(ViewMySties));
		}

		[Route("user/mysites/site/cookies/init")]
		public IActionResult InitCookies()
		{	
			var cookies = new List<SiteCookieM>();
			cookies.Add(new SiteCookieM() { Name = "Google Analytics UserId" });
			cookies.Add(new SiteCookieM() { Name = "adform.net" });
			cookies.Add(new SiteCookieM() { Name = "track.adform.net" });
			cookies.Add(new SiteCookieM() { Name = "adnxs.com" });
			cookies.Add(new SiteCookieM() { Name = "brandmetrics.net" });
			cookies.Add(new SiteCookieM() { Name = "casalemedia.com" });
			cookies.Add(new SiteCookieM() { Name = "creativecdn.com" });
			cookies.Add(new SiteCookieM() { Name = "doubleclick.net" });
			cookies.Add(new SiteCookieM() { Name = "hit.gemius.pl" });
			cookies.Add(new SiteCookieM() { Name = "facebook.com" });
			cookies.Add(new SiteCookieM() { Name = "facebook.com" });
			cookies.Add(new SiteCookieM() { Name = "goodservices.com" });
			cookies.Add(new SiteCookieM() { Name = "csr.onet.com" });
			cookies.Add(new SiteCookieM() { Name = "tagger.opencloud.com" });
			cookies.Add(new SiteCookieM() { Name = "pubmatic.com" });
			cookies.Add(new SiteCookieM() { Name = "eus.rubiconproject.com" });

			foreach (var s in cookies)
			{
				SiteCookieM so = _db.SiteCookies.SingleOrDefault(a => a.Name == s.Name);
				if (so == null)
				{
					_db.Add(s);
					_db.SaveChanges();
				}
			}

			return RedirectToAction(nameof(ViewMySite));
		}
	}
}
