﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoldBug.Data;
using GoldBug.Models;
using Microsoft.AspNetCore.Mvc;


namespace GoldBug.Controllers
{
	public class UserAlertsController : Controller
	{

		private readonly ApplicationDbContext _db;

		public UserAlertsController(ApplicationDbContext db)
		{
			_db = db;
		}

		[Route("user/myalerts")]
		public IActionResult ViewMyAlerts()
		{
			return View();
		}

		[Route("user/myalerts/init")]
		public IActionResult Init()
		{
			var alertsList = new List<AlertM>();
			alertsList.Add(new AlertM() { Name = "Strona zgłoszona jako niebezpieczna", CriticaLevel = 9, Site = "www.facebook.com" });
			alertsList.Add(new AlertM() { Name = "Strona zawiera treści pornograficzne", CriticaLevel = 6, Site = "www.onet.pl" });
			alertsList.Add(new AlertM() { Name = "Brak protokołu HTTPS", CriticaLevel = 5, Site = "www.itaka.pl" });
			alertsList.Add(new AlertM() { Name = "Brak protokołu HTTPS", CriticaLevel = 5, Site = "www.itaka.pl" });
			alertsList.Add(new AlertM() { Name = "Strona zgłoszona jako niebezpieczna", CriticaLevel = 3, Site = "app.hrappka.pl" });
			alertsList.Add(new AlertM() { Name = "Brak protokołu HTTPS", CriticaLevel = 1, Site = "www.morele.net" });
			alertsList.Add(new AlertM() { Name = "Strona zgłoszona jako niebezpieczna", CriticaLevel = 1, Site = "www.pyszne.pl" });

			foreach (var s in alertsList)
			{
					_db.Add(s);
					_db.SaveChanges();
			}

			return RedirectToAction(nameof(ViewMyAlerts));
		}

		[Route("user/myalerts/update")]
		public IActionResult Update()
		{
			var alertsList = _db.Alerts.ToList();
			foreach (var s in alertsList)
			{
				var site = _db.UserSites.FirstOrDefault(a => a.Domain == s.Site);
				if (site != null)
				{
					s.UserSiteId = site.Id;
					_db.Update(s);
					_db.SaveChanges();
				}

			}
			return RedirectToAction(nameof(ViewMyAlerts));
		}
	}
}
