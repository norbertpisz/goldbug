﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace GoldBug.Controllers
{
    public class UserPanelController : Controller
    {
        [Route("user/dashboard")]
        public IActionResult Dashboard()
        {
            return View();
        }
    }
}
