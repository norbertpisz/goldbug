const chars = {
	letters: 'qwertyuiopasdfghjklzxcvbn',
	capitals: 'QWERTYUIOPASDFGHJKLZXCVBNM',
	digits: '1234567890',
	special: '`~!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?'
}

getRandomInt: function(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}

generatePassword: (length, charset) => {
	let password = '';
	for (let i = 0; i < length; i++) {
		password += charset[helper.getRandomInt(0, charset.length)]
	}
	return password;
}

generateCharSet: (hasCaps, hasDigits, hasSpecial, chars) => {
	let set = chars.letters;
	if (hasCaps) set += chars.capitals;
	//for similar propability
	if (hasDigits) set += chars.digits + chars.digits;
	if (hasSpecial) set += chars.special;
	return set;
}

