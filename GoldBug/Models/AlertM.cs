﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GoldBug.Models
{
	public class AlertM
	{
        public long Id { get; set; }

		public string Name { get; set;}
		public string Description { get; set; }
		public int CriticaLevel { get; set; }
		public string Action { get; set; }

        public string Site { get; set; }
        [ForeignKey(nameof(UserSiteId))]
        public UserSiteM UserSite { get; set; }
        public long? UserSiteId { get; set; }
    }
}