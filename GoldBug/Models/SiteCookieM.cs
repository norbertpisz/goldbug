﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GoldBug.Models
{
    public class SiteCookieM
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        [ForeignKey(nameof(UserSiteId))]
        public UserSiteM UserSite { get; set; }
        public long? UserSiteId { get; set; }
    }
}
