﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GoldBug.Models
{
	public class SecurityRuleM
	{
        public long Id { get; set; }

		public string Name { get; set; }
		public string Site { get; set; }
        public string RuleCode { get; set; }
        public string TrackerCode { get; set; }
        public string RuleRegex { get; set; }

        [ForeignKey(nameof(UserSiteId))]
        public UserSiteM UserSite { get; set; }
        public long? UserSiteId { get; set; }
	}
}
