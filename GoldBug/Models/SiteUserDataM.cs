﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GoldBug.Models
{
    public class SiteUserDataM
    {
        public long Id { get; set; }
        public string Name { get; set; }
		public string IconName { get; set; }
        public bool IsProvided { get; set; }
        public bool IsBlocked { get; set; }

        [ForeignKey(nameof(UserSiteId))]
        public UserSiteM UserSite { get; set; }
        public long? UserSiteId { get; set; }
    }
}
