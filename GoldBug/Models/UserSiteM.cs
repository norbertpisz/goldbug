﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoldBug.Models
{
    public class UserSiteM
    {
        public long Id { get; set; }

        public string Domain { get; set; }
        public string DomainCategory { get; set; }

        public DateTime LastViewed { get; set; }
        public List<string> TrackingScripts { get; set; }
        public List<string> ListData { get; set; }
		public string LastVisited { get; set; }
		public int Views { get; set; }
    }
}
