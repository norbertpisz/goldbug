﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace GoldBug.Migrations
{
    public partial class Initialmodels2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "UserSiteId",
                table: "WordSearches",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserSiteId",
                table: "SiteCookies",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserSiteId",
                table: "Alerts",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SiteSecurityRules",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    Site = table.Column<string>(nullable: true),
                    UserSiteId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiteSecurityRules", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SiteSecurityRules_UserSites_UserSiteId",
                        column: x => x.UserSiteId,
                        principalTable: "UserSites",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SiteTracker",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    IsEnabled = table.Column<bool>(nullable: false),
                    IsBlocked = table.Column<bool>(nullable: false),
                    UserSiteId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiteTracker", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SiteTracker_UserSites_UserSiteId",
                        column: x => x.UserSiteId,
                        principalTable: "UserSites",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SiteUserData",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    IconName = table.Column<string>(nullable: true),
                    IsProvided = table.Column<bool>(nullable: false),
                    IsBlocked = table.Column<bool>(nullable: false),
                    UserSiteId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiteUserData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SiteUserData_UserSites_UserSiteId",
                        column: x => x.UserSiteId,
                        principalTable: "UserSites",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WordSearches_UserSiteId",
                table: "WordSearches",
                column: "UserSiteId");

            migrationBuilder.CreateIndex(
                name: "IX_SiteCookies_UserSiteId",
                table: "SiteCookies",
                column: "UserSiteId");

            migrationBuilder.CreateIndex(
                name: "IX_Alerts_UserSiteId",
                table: "Alerts",
                column: "UserSiteId");

            migrationBuilder.CreateIndex(
                name: "IX_SiteSecurityRules_UserSiteId",
                table: "SiteSecurityRules",
                column: "UserSiteId");

            migrationBuilder.CreateIndex(
                name: "IX_SiteTracker_UserSiteId",
                table: "SiteTracker",
                column: "UserSiteId");

            migrationBuilder.CreateIndex(
                name: "IX_SiteUserData_UserSiteId",
                table: "SiteUserData",
                column: "UserSiteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Alerts_UserSites_UserSiteId",
                table: "Alerts",
                column: "UserSiteId",
                principalTable: "UserSites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SiteCookies_UserSites_UserSiteId",
                table: "SiteCookies",
                column: "UserSiteId",
                principalTable: "UserSites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WordSearches_UserSites_UserSiteId",
                table: "WordSearches",
                column: "UserSiteId",
                principalTable: "UserSites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Alerts_UserSites_UserSiteId",
                table: "Alerts");

            migrationBuilder.DropForeignKey(
                name: "FK_SiteCookies_UserSites_UserSiteId",
                table: "SiteCookies");

            migrationBuilder.DropForeignKey(
                name: "FK_WordSearches_UserSites_UserSiteId",
                table: "WordSearches");

            migrationBuilder.DropTable(
                name: "SiteSecurityRules");

            migrationBuilder.DropTable(
                name: "SiteTracker");

            migrationBuilder.DropTable(
                name: "SiteUserData");

            migrationBuilder.DropIndex(
                name: "IX_WordSearches_UserSiteId",
                table: "WordSearches");

            migrationBuilder.DropIndex(
                name: "IX_SiteCookies_UserSiteId",
                table: "SiteCookies");

            migrationBuilder.DropIndex(
                name: "IX_Alerts_UserSiteId",
                table: "Alerts");

            migrationBuilder.DropColumn(
                name: "UserSiteId",
                table: "WordSearches");

            migrationBuilder.DropColumn(
                name: "UserSiteId",
                table: "SiteCookies");

            migrationBuilder.DropColumn(
                name: "UserSiteId",
                table: "Alerts");
        }
    }
}
