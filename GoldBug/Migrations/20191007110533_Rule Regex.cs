﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GoldBug.Migrations
{
    public partial class RuleRegex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RuleRegex",
                table: "SiteTracker",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RuleRegex",
                table: "SiteSecurityRules",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RuleRegex",
                table: "SiteTracker");

            migrationBuilder.DropColumn(
                name: "RuleRegex",
                table: "SiteSecurityRules");
        }
    }
}
