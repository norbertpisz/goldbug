﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GoldBug.Migrations
{
    public partial class Addrulessecuritytosite : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TrackerCode",
                table: "SiteTracker",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RuleCode",
                table: "SiteSecurityRules",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TrackerCode",
                table: "SiteSecurityRules",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TrackerCode",
                table: "SiteTracker");

            migrationBuilder.DropColumn(
                name: "RuleCode",
                table: "SiteSecurityRules");

            migrationBuilder.DropColumn(
                name: "TrackerCode",
                table: "SiteSecurityRules");
        }
    }
}
