﻿using System;
using System.Collections.Generic;
using System.Text;
using GoldBug.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace GoldBug.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<UserSiteM> UserSites { get; set; }
        public DbSet<AlertM> Alerts { get; set; }
        public DbSet<NotificationM> Notifications { get; set; }
        public DbSet<SiteCookieM> SiteCookies { get; set; }
        public DbSet<WordSearchM> WordSearches { get; set; }
        public DbSet<SiteUserDataM> SiteUserData { get; set; }
        public DbSet<SecurityRuleM> SiteSecurityRules { get; set; }
        public DbSet<TrackerM> SiteTracker { get; set; }


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
			Database.Migrate();
        }
    }
}
